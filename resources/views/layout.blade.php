<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X_UA_Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		{{-- Include the WebComponents polyfill --}}
		<script src="{{ asset('/js/webcomponents-lite.min.js') }}"></script>

		{{-- Include any custom elements here --}}
		@yield('elements')
	</head>
	<body>
		<h1>Larymer</h1>
		@yield('content')
	</body>
</html>
