@extends('polymer-template')

@section('dom-module')
	<dom-module id="custom-element">
		<style>
			:host .section-header {
				color: #f00;
			}
		</style>
		<template>
			<h1 class="section-header">[[slug]]</h1>
		</template>
		<script>
			Polymer({
				is: 'custom-element',
				properties: {
					slug: String,
				},
				ready: function () {
					var tag = this.getElementsByTagName("h1")[0],
					    transformedSlug = this.slug.replace(" ", "-").toLowerCase();

					tag.id = transformedSlug;
				},
			});
		</script>
	</dom-module>
@endsection
